package com.android.widgetlist;

import android.app.Activity;
import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.UserHandle;
import android.os.UserManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends Activity implements WidgetSelectListner {
    static final int APPWIDGET_HOST_ID = 2186;
    private static final int REQUEST_CREATE_APPWIDGET = 5;

    private static final int REQUEST_PICK_APPWIDGET = 9;
    private static final int REQUEST_BIND_APPWIDGET = 11;
    ArrayList<WidgetModel> packageMap = new ArrayList<>();
    AppWidgetManager mAppWidgetManager;
    ListView listView;
    int cols = 4;
    int rows = 4;
    float cellWidth = 200;
    float cellHeight = 200;
    MyWidgetHostView hostView;
    int appWidgetId;
    AppWidgetProviderInfo appWidgetInfo;
   // private AppWidgetHostView widgets[] = new AppWidgetHostView[16];
    private MyAppWidgetHost mAppWidgetHost;
    private int widgCount = 0;
    private LinearLayout widgetView;
    public HashMap<String, ArrayList<AppWidgetProviderInfo>> appWidgetProviderInfoMap = new HashMap<>();
    public ArrayList<String> packageNamesList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widgets_view);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int densityDpi = (int) (metrics.density * 160f);
        cellWidth = Utilities.pxFromDp(70, metrics);
        cellHeight = Utilities.pxFromDp(70, metrics);
        mAppWidgetManager = AppWidgetManager.getInstance(this);
        mAppWidgetHost = new MyAppWidgetHost(this, APPWIDGET_HOST_ID);

        widgetView = (LinearLayout) findViewById(R.id.widget_container);

        LinkedList<AppWidgetProviderInfo> linkedList = new LinkedList<>();
        AppWidgetManager instance = AppWidgetManager.getInstance(this);
        for (UserHandle installedProvidersForProfile : getUserHandleList((Context) this)) {
            linkedList.addAll(instance.getInstalledProvidersForProfile(installedProvidersForProfile));
        }
        //List<AppWidgetProviderInfo> infoList = mAppWidgetManager.getInstalledProviders();

        final PackageManager pm = getApplicationContext().getPackageManager();
        String lastPackage = "";

        for (AppWidgetProviderInfo info : linkedList) {
            WidgetModel widgetModel = new WidgetModel();
            WidgetItem widgetItem = new WidgetItem();

            widgetItem.lable = info.loadLabel(pm);

            try {

                ApplicationInfo ai;
                try {
                    ai = pm.getApplicationInfo(info.provider.getPackageName(), 0);
                } catch (final PackageManager.NameNotFoundException e) {
                    ai = null;
                }
                widgetModel.appName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
                widgetModel.icon = pm.getApplicationIcon(info.provider.getPackageName());

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            Rect widgetPadding = AppWidgetHostView.getDefaultPaddingForWidget(
                    this, info.provider, null);

            int spanX = Math.max(1, (int) Math.ceil(
                    (info.minWidth + widgetPadding.left + widgetPadding.right) / cellWidth));

            int spanY = Math.max(1, (int) Math.ceil(
                    (info.minHeight + widgetPadding.top + widgetPadding.bottom) / cellHeight));

            widgetItem.hSpan = Math.min(spanX, cols);
            widgetItem.vSpan = Math.min(spanY, rows);
            widgetItem.image = info.loadPreviewImage(this, densityDpi);
            widgetItem.minHeigh = info.minHeight;
            widgetItem.minWidth = info.minWidth;
            widgetItem.minResizeHeigh = info.minResizeHeight;
            widgetItem.minResizeWidth = info.minResizeWidth;
            widgetItem.resizeMode = info.resizeMode;
            widgetItem.info = info;

            if (lastPackage.equals(info.provider.getPackageName())) {
                packageMap.get(packageMap.size() - 1).widgetItems.add(widgetItem);
            } else {
                widgetModel.widgetItems.add(widgetItem);
                packageMap.add(widgetModel);
            }

            lastPackage = info.provider.getPackageName();

            for (AppWidgetProviderInfo appWidgetProviderInfoMap : linkedList) {
                String packageName = appWidgetProviderInfoMap.provider.getPackageName();
                if (!packageName.startsWith("com.huawei.android.totemweather")) {
                    if (!this.appWidgetProviderInfoMap.containsKey(packageName)) {
                        this.packageNamesList.add(packageName);
                        this.appWidgetProviderInfoMap.put(packageName, new ArrayList());
                    }
                    this.appWidgetProviderInfoMap.get(packageName).add(appWidgetProviderInfoMap);
                }
            }
        }

        listView = (ListView) findViewById(R.id.widgets_list_view);

        ListAdapter mAdapter = new ListAdapter(this, packageMap, this);
        listView.setAdapter(mAdapter);

    }
    public List<UserHandle> getUserHandleList(Context context) {
        return ((UserManager) Objects.requireNonNull(context.getSystemService(Context.USER_SERVICE))).getUserProfiles();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAppWidgetHost.startListening();
    }

    @Override
    public void onWidgetSelect(int pos, int subIndex) {
        appWidgetId = mAppWidgetHost.allocateAppWidgetId();

        appWidgetInfo = packageMap.get(pos).widgetItems.get(subIndex).info;
      //  boolean success = mAppWidgetManager.bindAppWidgetIdIfAllowed(appWidgetId, appWidgetInfo.getProfile(), appWidgetInfo.provider, null);
        boolean success = mAppWidgetManager.bindAppWidgetIdIfAllowed(
                appWidgetId,appWidgetInfo.getProfile(), appWidgetInfo.provider,getDefaultOptionsForWidget(this,appWidgetInfo));
        if (success ) {
            configureWidget(appWidgetInfo, appWidgetId, widgetView);
        } else {

            Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_BIND)
                    .putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
                    .putExtra(AppWidgetManager.EXTRA_APPWIDGET_PROVIDER, appWidgetInfo.provider)
                    .putExtra(AppWidgetManager.EXTRA_APPWIDGET_PROVIDER_PROFILE, appWidgetInfo.getProfile());


       /*     Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_BIND);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_PROVIDER, appWidgetInfo.provider);
            UserHandleCompat userHandleCompat = UserHandleCompat.myUserHandle();

            userHandleCompat.addToIntent(intent, AppWidgetManager.EXTRA_APPWIDGET_PROVIDER_PROFILE);*/
            // TODO: we need to make sure that this accounts for the options bundle.
            // intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_OPTIONS, options);
            //mAppWidgetHost.startAppWidgetConfigureActivityForResult(this, appWidgetId, 0, REQUEST_BIND_APPWIDGET, null);
           startActivityForResult(intent, REQUEST_BIND_APPWIDGET);
        }

    }
    public String f12739H;
    public int f12755g;
    public List<?> mo11893A() {
        return this.f12739H.length() == 0 ? this.packageNamesList : (List) this.appWidgetProviderInfoMap.get(this.f12739H);
    }
    public  void onWidgetClick(View view) {
        ArrayList arrayList;
        int i;
        Object obj = MainActivity.this.mo11893A().get(this.f12755g);
        if (!(obj instanceof String) || ((ArrayList) Objects.requireNonNull(MainActivity.this.appWidgetProviderInfoMap.get(obj))).size() <= 1) {
            if (MainActivity.this.f12739H.length() == 0) {
                MainActivity pickWidgetActivity = MainActivity.this;
                arrayList = (ArrayList) pickWidgetActivity.appWidgetProviderInfoMap.get(pickWidgetActivity.packageNamesList.get(this.f12755g));
                i = 0;
            } else {
                MainActivity pickWidgetActivity2 = MainActivity.this;
                arrayList = (ArrayList) pickWidgetActivity2.appWidgetProviderInfoMap.get(pickWidgetActivity2.f12739H);
                i = this.f12755g;
            }
            AppWidgetProviderInfo appWidgetProviderInfo = (AppWidgetProviderInfo) ((ArrayList) Objects.requireNonNull(arrayList)).get(i);
            String str = "appWidgetId";
            int intExtra = MainActivity.this.getIntent().getIntExtra(str, -1);
            if (MainActivity.this.bindAppWidgetIdIfAllowed(appWidgetProviderInfo, intExtra)) {
                Intent intent = new Intent();
                intent.putExtra(str, intExtra);
                MainActivity.this.setResult(-1, intent);
                MainActivity.this.finish();
                return;
            }
            Intent intent2 = new Intent("android.appwidget.action.APPWIDGET_BIND");
            intent2.putExtra(str, intExtra);
            intent2.putExtra("appWidgetProvider", appWidgetProviderInfo.provider);
            MainActivity.this.startActivityForResult(intent2, 5566);
            return;
        }

    }
    public boolean bindAppWidgetIdIfAllowed(AppWidgetProviderInfo appWidgetProviderInfo, int i) {
        return this.mAppWidgetManager.bindAppWidgetIdIfAllowed(i, appWidgetProviderInfo.getProfile(), appWidgetProviderInfo.provider, null);
    }
    public static Bundle getDefaultOptionsForWidget(Activity launcher, AppWidgetProviderInfo info) {
        Bundle options = null;
        Rect rect = new Rect();
        if (Utilities.ATLEAST_JB_MR1) {
         //   AppWidgetResizeFrame.getWidgetSizeRanges(launcher, info.spanX, info.spanY, rect);
            Rect padding = AppWidgetHostView.getDefaultPaddingForWidget(launcher,
                    info.provider, null);

            float density = launcher.getResources().getDisplayMetrics().density;
            int xPaddingDips = (int) ((padding.left + padding.right) / density);
            int yPaddingDips = (int) ((padding.top + padding.bottom) / density);

            options = new Bundle();
            options.putInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH,
                    rect.left - xPaddingDips);
            options.putInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT,
                    rect.top - yPaddingDips);
            options.putInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH,
                    rect.right - xPaddingDips);
            options.putInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT,
                    rect.bottom - yPaddingDips);
        }
        return options;
    }
    @Override

    public void onDestroy() {

        super.onDestroy();

        try {

            mAppWidgetHost.stopListening();


        } catch (NullPointerException ex) {

            Log.w("lockscreen destroy", "problem while stopping AppWidgetHost during Lockscreen destruction", ex);

        }

    }


    private void configureWidget(AppWidgetProviderInfo appWidgetInfo, int appWidgetId, View v1) {


        if (appWidgetInfo.configure != null) {
            Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
            intent.setComponent(appWidgetInfo.configure);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            mAppWidgetHost.startAppWidgetConfigureActivityForResult(this, appWidgetId, 0, REQUEST_CREATE_APPWIDGET, null);
            //startActivityForResult(intent, REQUEST_CREATE_APPWIDGET);


        } else {

            createWidget(appWidgetInfo, appWidgetId, v1);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == REQUEST_CREATE_APPWIDGET && resultCode == RESULT_OK) {

                createWidget(appWidgetInfo, appWidgetId, widgetView);


            } else if ((requestCode == REQUEST_CREATE_APPWIDGET || requestCode == REQUEST_PICK_APPWIDGET) && resultCode == RESULT_CANCELED && data != null) {
                int appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
                if (appWidgetId != -1) {
                    mAppWidgetHost.deleteAppWidgetId(appWidgetId);
                }
            } else if (requestCode == REQUEST_BIND_APPWIDGET) {
                // This is called only if the user did not previously have permissions to bind widgets

                if (resultCode == 0) {
                /*    boolean success =   mAppWidgetManager.bindAppWidgetIdIfAllowed(
                            appWidgetId, appWidgetInfo.provider);*/
                    createWidget(appWidgetInfo, appWidgetId, widgetView);
                    /*if(success) {

                    }else{
                        Toast.makeText(MainActivity.this,"Error",Toast.LENGTH_SHORT).show();
                    }*/

                    // When the user has granted permission to bind widgets, we should check to see if
                    // we can inflate the default search bar widget.

                }
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
    int count=0;
    public void createWidget(AppWidgetProviderInfo appWidgetInfo, int appWidgetId, View layout1) {


        hostView = (MyWidgetHostView) mAppWidgetHost.createView(this, appWidgetId, appWidgetInfo);


        hostView.setAppWidget(appWidgetId, appWidgetInfo);
        hostView.setLayoutParams(new LinearLayout.LayoutParams(widgetView.getWidth(), widgetView.getHeight()));

        hostView.requestLayout();
        hostView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
              //  Toast.makeText(MainActivity.this,"Long Click", Toast.LENGTH_LONG).show();
             Log.i("count",++count+"");
                return false;
            }
        });

        widgetView = (LinearLayout) layout1;
        widgetView.removeAllViews();
        widgetView.addView(hostView);



    }

}
