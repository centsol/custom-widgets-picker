package com.android.widgetlist;

import android.appwidget.AppWidgetHostView;
import android.content.Context;
import android.view.MotionEvent;

public class MyWidgetHostView extends AppWidgetHostView {

    /* renamed from: b */
    public OnTouchListener onTouchListener;

    /* renamed from: c */
    public OnLongClickListener onLongClickListener;

    /* renamed from: d */
    public long longPressTime;

    public MyWidgetHostView(Context context) {
        super(context);
    }
    boolean isLongPressCalled =false;
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if(motionEvent.getAction()==MotionEvent.ACTION_DOWN){
            isLongPressCalled =false;
        }
        OnTouchListener onTouchListener = this.onTouchListener;
        if (onTouchListener != null) {
            onTouchListener.onTouch(this, motionEvent);
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.longPressTime = System.currentTimeMillis();
        } else if (actionMasked == 2 && System.currentTimeMillis() - this.longPressTime > 300) {
            if(!isLongPressCalled) {
                this.onLongClickListener.onLongClick(this);
                isLongPressCalled =true;
            }
        }
        return false;
    }

    public void setOnLongClickListener(OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.onTouchListener = onTouchListener;
    }

}
