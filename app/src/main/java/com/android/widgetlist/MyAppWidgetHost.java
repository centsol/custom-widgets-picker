package com.android.widgetlist;

import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;

public class MyAppWidgetHost extends AppWidgetHost {
    public MyAppWidgetHost(Context context, int i) {
        super(context, i);
    }

    public AppWidgetHostView onCreateView(Context context, int i, AppWidgetProviderInfo appWidgetProviderInfo) {
        return new MyWidgetHostView(context);
    }

}
